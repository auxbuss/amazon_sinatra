# encoding: utf-8

module Books
  class Main < Sinatra::Base

    configure :development do
      register Sinatra::Reloader
      set :session_secret, "something"
      use BetterErrors::Middleware
      BetterErrors.application_root = File.expand_path("..", __FILE__)
    end

    configure do
      set :public_folder, 'public'
      use Rack::Session::Pool
    end

    helpers do
      def partial(page, options={})
        haml page, options.merge!(:layout => false)
      end
    end

    before do
      @country_code = :uk
      @amazon = Amazon.new(@country_code)
      session[:search] = AmazonBestSeller.get_best_sellers(@country_code) if ! session[:search]
      session[:history] = {} if ! session[:history]
    end

    get '/' do
      session[:search] = AmazonBestSeller.get_best_sellers(@country_code)
      @book = Book.get_asin(session[:search][0][:asin])
      session[:history][@book.asin] = { asin: @book.asin, title: @book.title, authors: @book.authors }
      haml :index, :locals => {search: session[:search], history: session[:history], home_page: true}
    end

    get '/book/:asin' do
      @book = @amazon.create_book(params[:asin]) if ! @book = Book.get_asin(params[:asin])
      @book = @amazon.recreate_book(@book) if @book && @book.out_of_date
      halt haml :missing_book, :locals => {search: session[:search], history: session[:history]} unless @book
      session[:history][@book.asin] = { asin: @book.asin, title: @book.title, authors: @book.authors }
      haml :index, :locals => {search: session[:search], history: session[:history], home_page: false}
    end

    post "/search" do
      search_results = @amazon.search_for_books(params[:keywords], 1)
      session[:search] = search_results[:books]
      haml :_search_results, layout: false, :locals => { search: session[:search] }
    end

    post '/clear_history' do
      session[:history] = {}
    end

    get "/amazon" do
      response = @amazon.search('Books', { Keywords: 'marc cooper', ItemPage: 1 } )
      result = response.xml
      haml :debug, layout: false, :locals => { result: result }
    end

    get "/asin/:asin" do
      response = @amazon.find(params[:asin], response_group: 'Large')
      result = response.xml
      haml :debug, layout: false, :locals => { result: result }
    end

    get "/author/:author" do
      response = @amazon.search('Books', { Keywords: params[:author], ItemPage: 1, ResponseGroup: 'Large'})
      result = response.xml
      haml :debug, layout: false, :locals => { result: result }
    end

    get "/similar/:asin" do
      response = @amazon.find_similar(params[:asin])
      result = response.xml
      haml :debug, layout: false, :locals => { result: result }
    end

    not_found do
      redirect to('/')
    end

    error do
      redirect to('/')
    end

  end
end
