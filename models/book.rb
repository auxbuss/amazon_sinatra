# encoding: utf-8

class Book
  include Mongoid::Document
  include Mongoid::Timestamps

  field :asin,   :type => String
  field :title,  :type => String
  field :authors, :type => Array
  field :image,  :type => String
  field :description, :type => String
  field :reviews_url, :type => String
  field :type, :type => String
  field :suggested, :type => Array
  field :list_price, type: String
  field :offer_price, type: String
  field :currency, type: String
  field :page_count, type: String
  field :publication_date, type: String
  field :amazon_url, type: String

  validates_length_of :asin, :within => 10..10
  validates_uniqueness_of :asin
  validates_presence_of :title, :authors, :image, :description, :reviews_url, :type, :amazon_url

  RENEWAL_LIMIT = 12

  def self.get_asin(asin)
    Book.find_by(asin: asin)
  end

  def self.valid_product_group? product_group
    ['Book', 'eBooks'].index product_group
  end

  def out_of_date
    Time.now - updated_at > RENEWAL_LIMIT.hours
  end

  def get_display_price
    return offer_price unless offer_price == ''
    list_price
  end

  def self.get_offer_price(book)
    return '' if book['Offers']['TotalOffers'] == "0"
    book['Offers']['Offer']['OfferListing']['Price'] ?
      book['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'] : ''
  end

  def self.get_authors(book)
    authors = Array(book['Items']['Item']['ItemAttributes']['Author'])
    authors.empty? ? authors = ['Unknown'] : authors
  end

  def self.add(book, suggestions = nil)
    book_item = book['Items']['Item']
    page_count = book_item['ItemAttributes']['NumberOfPages'] ? book_item['ItemAttributes']['NumberOfPages'] : 0
    if book_item['ItemAttributes']['Binding']['Kindle']
      offer_price = list_price = currency = ''
    else
      offer_price = get_offer_price(book_item)
      list_price = book_item['ItemAttributes']['ListPrice'] ? book_item['ItemAttributes']['ListPrice']['FormattedPrice'] : ''
      currency = book_item['ItemAttributes']['ListPrice'] ? book_item['ItemAttributes']['ListPrice']['CurrencyCode'] : ''
    end
    begin
      Book.create!(
        asin:         book_item['ASIN'],
        title:        book_item['ItemAttributes']['Title'],
        authors:      get_authors(book),
        image:        grab_image(book_item),
        description:  grab_description(book_item),
        reviews_url:  book_item['CustomerReviews']['IFrameURL'],
        amazon_url:   book_item['DetailPageURL'],
        type:         book_item['ItemAttributes']['Binding'],
        list_price:   list_price,
        offer_price:  offer_price,
        currency:     currency,
        page_count:   page_count,
        pub_date:     book_item['ItemAttributes']['PublicationDate'],
        suggested:    suggestions == nil ? [] : grab_suggested(suggestions)
      )
    rescue => exception
      puts "Book::add : #{$!}"
      puts "Book::add : failed to add asin: #{book_item['ASIN']}"
      nil
    end
  end

  def self.grab_image(book)
    if ! book['ImageSets']
      "http://totallybookreviews/images/no_image.jpg"
    elsif book['ImageSets']['ImageSet'][0]
      book['ImageSets']['ImageSet'][0]['TinyImage']['URL']
    else
      book['ImageSets']['ImageSet']['TinyImage']['URL']
    end
  end

  def self.grab_suggested(suggestions)
    suggested = []
    suggestions.each do |item|
      if valid_product_group? item['ItemAttributes']['ProductGroup']
        suggested << { "asin" => item['ASIN'], "title" => item['ItemAttributes']['Title'], "authors" => Array(item['ItemAttributes']['Author']) }
      else
        p "#{item['ASIN']}: invalid suggestion #{item['ItemAttributes']['ProductGroup']}"
      end
    end
    suggested
  end

  def self.grab_description(book)
    if ! book['EditorialReviews']
      'No description available.'
    elsif book['EditorialReviews']['EditorialReview'].class == Array
      book['EditorialReviews']['EditorialReview'][0]['Content']
    else
      book['EditorialReviews']['EditorialReview']['Content']
    end
  end

end
