class Author

  attr_reader :name, :total_books, :total_pages

  def initialize(name)
    @name = name
    @amazon_books = []
  end

  def add_amazon_books(books, page)
    @total_books = books['TotalResults'][0].to_i
    @total_pages = books['TotalPages'][0].to_i
    @amazon_books[page - 1] = books.map('Item') do |book|
      { asin: book['ASIN'], title: book['ItemAttributes']['Title'],
        author: book['ItemAttributes']['Author'], binding: book['ItemAttributes']['Binding']
      }
    end
  end

  def get_amazon_books(page)
    result = { total_books: @total_books, total_pages: @total_pages, books: [] }
    @amazon_books[page - 1].each do |book|
      result[:books] << { asin: book[:asin], author: book[:author], title: book[:title], binding: book[:binding]}
    end
    result.to_json
  end

  private

  def valid_author?(authors)
    Array(authors).each { |author_name| return true if author_name =~ /#{@name}/i }
    false
  end

end
