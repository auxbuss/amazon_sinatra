class GoodreadRecentReviews
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :books,  :type => Array
  
  def self.get_recent_reviews
    return nil unless reviews = first
    reviews.books
  end  

end
