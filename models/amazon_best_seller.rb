class AmazonBestSeller

  include Mongoid::Document
  include Mongoid::Timestamps

  field :locale, :type => String
  field :books,  :type => Array

  validates_length_of :locale, :within => 2..2

  def self.find_by_locale(locale)
    find_by(locale: locale)
  end

  def self.get_best_sellers(locale)
    return nil unless best_sellers = find_by_locale(locale)
    best_sellers.books.map do |asin|
      begin
        bestseller = Book.get_asin(asin)
        { asin: bestseller.asin, title: bestseller.title, authors: bestseller.authors }
      rescue Mongoid::Errors::DocumentNotFound
        p "Missing book asin: #{asin}"
        raise
      end
    end
  end

end
