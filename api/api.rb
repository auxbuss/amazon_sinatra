# encoding: utf-8

module Books
  class Api < Sinatra::Base

    before do
      @country_code = :uk
      @amazon = Amazon.new(@country_code)
    end

    get "/" do
      "Hello API!"
    end

    get "/author/:author/page/:page" do |author, page|
      content_type :json
      halt 404, {result: 'Author not found'}.to_json unless author = @amazon.get_author_with_books(author, page.to_i)
      author.get_amazon_books(page.to_i)
    end

    get "/geoip" do
      content_type :json
      request.location.data.to_json
    end

    get "/geoip/:ip" do |ip|
      content_type :json
      result = Geocoder.search(ip).first
      result.data.to_json
    end

    get "/asin/:asin" do
      content_type :json
      halt 404, 'ASIN not found' unless book = Book.get_asin(params[:asin])
      { asin: book.asin,
        title: book.title,
        authors: book.authors,
        image: book.image,
        description: book.description,
        reviews_url: book.reviews_url,
        type: book.type,
        suggested: book.suggested
      }.to_json
    end

  end
end
