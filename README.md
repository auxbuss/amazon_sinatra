## Hacking on the Amazon API with Sinatra

This is a Sinatra project that I used for playing around with the
Amazon and Goodreads APIs.

For simplicity, I used delegation. This could be brittle in a
production environment.

### Features

* web-site that queries the Amazon API in realtime
* web services/API
* disqus comments
* google analytics
* google adsense
* tests

### Notes

The default set-up relies on there being a few bestsellers in the
database. You can populate this (from Amazon's bestsellers) by running
`scripts/reset_bestsellers.rb` followed by
`update_amazon_uk_best_sellers.rb`.

There is an example cron in `scripts/` that will periodically update the
bestsellers, if you wish.

This project uses a very old version of vacuum to access the Amazon API.

### Requires

* mongodb
* memcached
* Goodreads API key
* Amazon API key & secret
* disqus short name (see `views/_discus.haml`)
* google analytics account info (see `views/_google_analytics.haml`)
* goodle adsense account (see `views/index.haml`)

Put the keys in:

* `ENV['GOODREADS_API_KEY']`
* `ENV['AMAZON_API_KEY']`
* `ENV['AMAZON_API_SECRET']`
* `ENV['AMAZON_API_TAG']`

### Comments

I'd base this on bootstrap if I were starting today. I'd probably use redis instead of memcache
as it gives you more options to play with the data.

The Amazon API is ugly, particulalry its data structures. I guess that they are stuck with a
huge legacy system that works, so the impetus to change is not there.

Anyway, if you hack on this, please let me know, I'd be interested in what you build with it.
