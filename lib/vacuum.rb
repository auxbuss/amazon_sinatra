module Vacuum
  class Response
    
    def find_one(query)
      find(query)[0]
    end
    
    def invalid?
      ! valid? || has_errors? ? true : false
    end
    
  end
end
