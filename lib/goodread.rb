require 'delegate'

class Goodread < SimpleDelegator

  def initialize
    delegate = Goodreads.new(:api_key => ENV['GOODREADS_API_KEY'])
    super(delegate)
  end

  def update_recent_reviews
    return nil unless reviews = recent_reviews
    return nil unless reviews.count == 20
    return nil unless gr_reviews = GoodreadRecentReviews.first()
    gr_reviews.books = []
    reviews.each do |book|
      gr_reviews.books << book
    end
    gr_reviews.save
  end

end
