# encoding: utf-8

require 'delegate'

class Amazon < SimpleDelegator

  LOCALES = {
    :uk => { best_sellers_node: 266239},
    :us => { best_sellers_node: 'unknown'}
  }

  def initialize(locale = :uk)
    raise Vacuum::BadLocale unless LOCALES.include?(locale)
    @locale = locale
    request = Vacuum[locale]
    request.configure do |c|
      c.key = ENV['AMAZON_API_KEY']
      c.secret = ENV['AMAZON_API_SECRET']
      c.tag = ENV['AMAZON_API_TAG']
    end
    super(request)
  end

  def get_book(asin, response_group = 'Large')
    response = find(asin, :response_group => response_group)
    response.to_hash unless response.invalid?
  end

  def get_similar(asin)
    response = find_similar(asin)
    result = []
    response.invalid? ? nil : response.each('Item') { |item| result << item }
  end

  def create_book(asin)
    return nil unless amazon_book = get_book(asin)
    amazon_suggestions = nil unless amazon_suggestions = get_similar(asin)
    Book.add(amazon_book, amazon_suggestions)
  end

  def recreate_book(book)
    asin = book.asin
    book.delete
    create_book(asin)
  end

  def get_bestsellers
    response = find_browse_node(LOCALES[@locale][:best_sellers_node], :response_group => 'TopSellers')
    response['TopItem'] unless response.invalid?
  end

  def add_books(books)
    books.each do |book|
      db_book = create_book(book['ASIN']) unless db_book = Book.get_asin(book['ASIN'])
      recreate_book(db_book) if db_book.out_of_date
    end
  end

  def update_bestsellers
    return nil unless books = get_bestsellers
    return nil unless bestsellers = AmazonBestSeller.find_by_locale(@locale)
    add_books books
    bestsellers.books = []
    books.each { |book| bestsellers.books << book['ASIN'] }
    bestsellers.save
  end

  def search_for_books(keywords, page)
    response = raw_search_for_books keywords, page
    return { total_books: 0, total_pages: 0, books: [] } unless response
    books = []
    response.each('Item') do |book|
      books << {
        asin: book['ASIN'],
        title: book['ItemAttributes']['Title'],
        authors: Array(book['ItemAttributes']['Author']),
        pages: book['ItemAttributes']['NumberOfPages']
      } if book['ItemAttributes']['Author']
    end
    { total_books: response['TotalResults'][0].to_i, total_pages: response['TotalPages'][0].to_i, books: books}
  end

  def raw_search_for_books(keywords, page)
    response = search('Books', { Keywords: keywords, ItemPage: page, ResponseGroup: 'Large' } )
    response.invalid? ? nil : response
  end

  def raw_book_search(author_name, page)
    search('Books', { author: author_name, ItemPage: page, ResponseGroup: 'Large'})
  end

  def get_author_with_books(author_name, page)
    author_name.downcase!
    if author_name.ascii_only?
      cached(author_name.gsub(/\s/, '') + '/' + page.to_s) { get_books(author_name, page) }
    else
      get_books(author_name, page)
    end
  end

  private

  def get_books(author_name, page)
    books = raw_book_search(author_name, page)
    return nil if books.invalid?
    author = Author.new(author_name)
    author.add_amazon_books(books, page)
    author
  end

  def cached(cache_key)
    memcached = Dalli::Client.new(nil, {:expires_in => 6.hours, :compress => true})
    begin
      result = memcached.get(cache_key)
    rescue Dalli::RingError
      result = nil
    end
    return result if result
    result = yield
    begin
      memcached.set(cache_key, result)
    rescue Dalli::RingError
      nil
    end
    result
  end

end
