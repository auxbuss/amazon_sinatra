require './config/boot.rb'

run Rack::URLMap.new \
  "/"    => Books::Main,
  "/api" => Books::Api
