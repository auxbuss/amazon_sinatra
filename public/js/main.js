function search() {
  if ($('#search').find('input[name="keywords"]').val().length != 0) {
    $.post('/search', { keywords: $('#search').find('input[name="keywords"]').val() } , function(data) {
      $("#search_results").html(data)
    })
  }
}
function stopEvent(event) {
  event.preventDefault();
  event.stopPropagation();
  if ($.browser.msie) {
    event.originalEvent.keyCode = 0;
    event.originalEvent.cancelBubble = true;
    event.originalEvent.returnValue = false;
  }
}
function clear_history() {
  $.post('/clear_history', function() {
    $("#history").children().remove()
  })
}

window.___gcfg = {lang: 'en-GB'};
(function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
