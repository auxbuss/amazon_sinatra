require_relative "test_helper"

class MainTest < MiniTest::Unit::TestCase
  include Rack::Test::Methods

  def app
    Books::Main
  end

  def test_it_says_book_review
		get '/'
    assert last_response.ok?
    assert last_response.body.include?('Totally book reviews')
	end

  def test_it_gets_a_book
		get '/book/2222222222'
    assert last_response.ok?
    assert last_response.body.include?('2222222222')
    assert last_response.body.include?('Some title')
	end

  def test_it_adds_a_book
		get '/book/0679753354'
    assert last_response.ok?
    assert last_response.body.include?('0679753354')
    assert last_response.body.include?('The Order of Things')
    assert_kind_of(Book, book = Book.get_asin('0679753354'))
    assert_equal('The Order of Things (Vintage)', book.title)
    assert_equal(11, Book.count())
	end

  def setup;
    Book.delete_all
    AmazonBestSeller.delete_all
    bestsellers = AmazonBestSeller.new(locale: 'uk')
    bestsellers.save

    assert_kind_of(AmazonBestSeller, bestsellers = AmazonBestSeller.find_by_locale('uk'))
    bestsellers.books = []
    asins = ['0000000000', '1111551111', '2222222222', '3333333333', '4444444444',
             '5555555555', '6666666666', '7777777777', '8888888888', '9999999999']
    asins.each do |asin|
      book = Book.create!(
        asin: asin, authors: ['Fred Blogs'], title: 'Some title', description: 'Some description',
        image: 'some_pic.jpg', reviews_url: 'http://reviews.com/', type: 'book type', pub_date: '2009-07-15',
        suggested: [
          {"asin"=>"B002UP1SX6", "title"=>"Call The Midwife", "authors"=>["Jennifer Worth"]},
          {"asin"=>"B002U3CC3I", "title"=>"Shadows Of The Workhouse", "authors"=>["Jennifer Worth"]},
          {"asin"=>"B002U3CC3S", "title"=>"Farewell To The East End", "authors"=>["Jennifer Worth"]},
          {"asin"=>"B005QOIL0Q", "title"=>"Catching Babies", "authors"=>["Sheena Byrom"]},
          {"asin"=>"B003PPDICE", "title"=>"Twelve Babies on a Bike", "authors"=>["Dot May Dunn"]},
          {"asin"=>"B006ARE2M4", "title"=>"Three years in starch.", "authors"=>["Maud Harris"]},
          {"asin"=>"B005HWB3L6", "title"=>"Bread, Jam and a Borrowed Pram", "authors"=>["Dot May Dunn"]},
          {"asin"=>"B005GRDQS0", "title"=>"A Nurse in the 1950s.", "authors"=>["Brenda Allen"]},
          {"asin"=>"B0051FF0L8", "title"=>"A Nurse in Time", "authors"=>["Evelyn Prentis"]},
          {"asin"=>"B005TKD76U", "title"=>"A Nurse in Action", "authors"=>["Evelyn Prentis"]}
        ], amazon_url: 'some url'
      )
      bestsellers.books << book.asin
    end
    bestsellers.save
  end

end
