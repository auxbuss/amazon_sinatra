require_relative "test_helper"

class GoodreadsTest < MiniTest::Unit::TestCase

  def test_raise_exception_when_api_key_not_provided
    goodreads = Goodreads.new
    assert_raises(Goodreads::ConfigurationError) { goodreads.book_by_isbn('0307463745') }
  end

  def test_raise_unauthorized_when_api_key_not_valid
    VCR.use_cassette('invalid_key') do
      assert_kind_of(Goodreads::Client,  goodreads = Goodreads.new(:api_key => 'INVALID_KEY'))
      assert_raises(Goodreads::Unauthorized) { goodreads.book_by_isbn('0307463745') }
    end
  end

  def test_get_a_book
    goodreads = Goodreads.new(:api_key => 'htlGlsLliDU5OGxUhx0dOg')
    VCR.use_cassette('rework_by_isbn') do
      assert_kind_of(Hashie::Mash, @book = goodreads.book_by_isbn('0307463745'))
    end
  end

  def test_get_a_book_goodread
    goodreads = Goodread.new
    VCR.use_cassette('rework_by_isbn') do
      assert_kind_of(Hashie::Mash, @book = goodreads.book_by_isbn('0307463745'))
    end
  end

  def test_get_a_books_details
    goodreads = Goodreads.new(:api_key => 'htlGlsLliDU5OGxUhx0dOg')
    VCR.use_cassette('rework_by_isbn') do
      book = goodreads.book_by_isbn('0307463745')
      assert_kind_of(Hashie::Mash, book)
      assert_equal('Rework', book.title)
#      assert_equal('4.03', book.average_rating)
      assert_kind_of(Array, book.authors.author)
      assert_equal('Jason Fried', book.authors.author[0].name)
      assert_equal('David Heinemeier Hansson', book.authors.author[1].name)
    end
  end

  def test_get_recent_reviews
    goodreads = Goodreads.new(:api_key => 'htlGlsLliDU5OGxUhx0dOg')
    VCR.use_cassette('recent_reviews') do
      reviews = goodreads.recent_reviews()
      assert_kind_of(Array, reviews)
      assert_equal(20, reviews.count)
#      assert_equal("\n    Piggybook\n  ", reviews[0].book.title)
#      assert_equal("\n    Very Crazy, G.I.!: Strange but True Stories of the Vietnam War\n  ", reviews[1].book.title)
#      assert_equal("\n    The Statistical Probability of Love at First Sight\n  ", reviews[19].book.title)
      title = reviews[0].book.title
      assert_kind_of(String, title)
#      assert_equal('Piggybook', title.strip)
#      title = reviews[19].book.title
#      assert_equal('The Statistical Probability of Love at First Sight', title.strip)
    end
  end

  def test_get_review
    goodreads = Goodreads.new(:api_key => 'htlGlsLliDU5OGxUhx0dOg')
    VCR.use_cassette('one_review') do
      review = goodreads.review(267934651)
      assert_kind_of(Hashie::Mash, review)
      assert_equal("Piggybook", review.book.title)
    end
  end

  def test_author
    goodreads = Goodreads.new(:api_key => 'htlGlsLliDU5OGxUhx0dOg')
    VCR.use_cassette('author') do
      author = goodreads.author(3706)
      assert_equal('George Orwell', author.name)
      assert_equal(10, author.books.book.count)
#      assert_equal('148', author.works_count) # removed as keeps changing
    end
  end

  def test_pull_recent_reviews
    VCR.use_cassette('goodreads_recent_reviews') do
      reviews = @goodreads.recent_reviews
      assert_equal(Array, reviews.class)
      assert_equal(20, reviews.count)
#      assert_equal('Claire DeWitt and the City of the Dead', reviews[0].book.title.strip)
#      assert_equal('0547428499', reviews[0].book.isbn)
    end
  end

  def test_update_recent_reviews
    VCR.use_cassette('goodreads_recent_reviews') do
      assert_equal(true, @goodreads.update_recent_reviews)
    end
  end

  def setup
    Goodreads.reset_configuration
    assert_kind_of(Goodread, @goodreads = Goodread.new)
  end
end
