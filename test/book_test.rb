# encoding: utf-8

require_relative "test_helper"

class BookTest < MiniTest::Unit::TestCase

  def setup
    @amazon_uk = Amazon.new :uk
    Book.delete_all
    asins = ['0000000000', '1111111111', 'B222222222', '3333333333', '4444444444',
             '5555555555', '6666666666', '7777777777', 'H888888888', '9999999999']
    asins.each do |asin|
      Book.create!(
        asin: asin, authors: ['Fred Blogs'], title: 'Some title', description: 'Some description',
        image: 'some_pic.jpg', reviews_url: 'http://reviews.com/', type: 'book type', suggested: [1,2,3,4],
        amazon_url: 'some url'
      )
    end
  end

  def test_out_of_date
    assert_kind_of(Book, book = Book::get_asin(5555555555))
    assert_equal(false, book.out_of_date)
  end

  def test_for_missing_book
    refute_nil(Book.find_by(asin: 5555555555))
    assert_equal(nil, Book.find_by(asin: 1234567890))
    assert_equal('0000000000', Book.find_by(asin: '0000000000').asin)
    assert_equal('5555555555', Book.find_by(asin: '5555555555').asin)
    assert_equal('Some title', Book.find_by(asin: '0000000000').title)
    assert_equal('Some title', Book.find_by(asin: '5555555555').title)
    refute_nil(Book.get_asin(5555555555))
  end

  def test_create_book
    asin = '0385523882'
    VCR.use_cassette('buyology') do
      assert_kind_of(Hash, amazon_book = @amazon_uk.get_book(asin))
      assert_equal(asin, amazon_book['Items']['Item']['ASIN'])
      assert_kind_of(Array, amazon_suggestions = @amazon_uk.get_similar(asin))
      assert_equal(10, amazon_suggestions.count)
      assert_kind_of(Book, @amazon_uk.create_book(asin))
    end
  end

  def test_create_book_no_author
    asin = 'B004TSB6NO'
    VCR.use_cassette('japanese_literature') do
      assert_kind_of(Hash, amazon_book = @amazon_uk.get_book(asin))
      assert_equal(asin, amazon_book['Items']['Item']['ASIN'])
      assert_kind_of(Array, amazon_suggestions = @amazon_uk.get_similar(asin))
      assert_equal(10, amazon_suggestions.count)
      assert_kind_of(Book, book = @amazon_uk.create_book(asin))
      assert_equal('MURASAKI SHIKIB', book.authors[0])
    end
  end

  def test_create_book_no_image
    asin = '0140081437'
    VCR.use_cassette('close_to_home') do
      assert_kind_of(Hash, amazon_book = @amazon_uk.get_book(asin))
      assert_equal(asin, amazon_book['Items']['Item']['ASIN'])
      assert_equal(nil, @amazon_uk.get_similar(asin))
      assert_kind_of(Book, book = @amazon_uk.create_book(asin))
      assert_equal('http://totallybookreviews/images/no_image.jpg', book.image)
    end
  end

  def test_add_a_book_with_two_image_sets
    asin = '1407109081'
    VCR.use_cassette('hunger_games_paperback') do
      assert_equal(nil, Book.get_asin(asin))
      book = @amazon_uk.get_book(asin)
      assert_kind_of(Hash, book)
      suggestions = @amazon_uk.get_similar(asin)
      assert_kind_of(Array, suggestions)
      book = Book.add(book, suggestions)
      assert_kind_of(Book, book)
      assert_equal('The Hunger Games', book.title)
      assert_equal('Suzanne Collins', book.authors[0])
      assert_equal('http://ecx.images-amazon.com/images/I/51piuUu%2B1hL._SL110_.jpg', book.image)
      assert_equal('Paperback', book.type)
      assert_equal('£7.99', book.list_price)
      assert_equal('£3.86', book.offer_price)
      assert_equal('GBP', book.currency)
      assert_equal('464', book.page_count)
      assert_equal('2009-01-05', book.pub_date)
      assert_equal({"asin"=>"1407109367",
 "authors"=>["Suzanne Collins"],
 "title"=>"Catching Fire (Hunger Games, Book 2)"},
        book.suggested[0])
    end
  end

  def test_add_a_book_succeeds
    asin = '1407117068'
    VCR.use_cassette('infernal_devices_large') do
      assert_nil(Book.get_asin(asin))
      book = @amazon_uk.get_book(asin)
      assert_kind_of(Hash, book)
      suggestions = @amazon_uk.get_similar(asin)
      assert_kind_of(Array, suggestions)
      book = Book.add(book, suggestions)
      assert_kind_of(Book, book)
      assert_equal('Infernal Devices (Mortal Engines Quartet)', book.title)
      assert_equal('Philip Reeve', book.authors[0])
      assert_equal("http://ecx.images-amazon.com/images/I/51JjiAvtGbL._SL110_.jpg", book.image)
      assert_equal('Paperback', book.type)
      assert_equal('£6.99', book.list_price)
      assert_equal('', book.offer_price)
      assert_equal('GBP', book.currency)
      assert_equal('352', book.page_count)
      assert_equal("2010-04-05", book.pub_date)
      assert_equal({"asin"=>"1407110942",
 "authors"=>["Philip Reeve"],
 "title"=>"A Darkling Plain (Mortal Engines Quartet)"},
        book.suggested[0])
      book = nil
      assert_raises(NoMethodError) { book.title }
      book = Book.add(asin, @amazon_uk) if ! book = Book.get_asin(asin)
      assert_equal('Infernal Devices (Mortal Engines Quartet)', book.title)
    end
  end

  def test_add_an_ebook_succeeds
    asin = 'B00684EBC0'
    VCR.use_cassette('only_the_innocent_large') do
      assert_nil(Book.get_asin(asin))
      book = @amazon_uk.get_book(asin)
      assert_kind_of(Hash, book)
      suggestions = @amazon_uk.get_similar(asin)
      assert_kind_of(Array, suggestions)
      book = Book.add(book, suggestions)
      assert_kind_of(Book, book)
      assert_equal('Only the Innocent', book.title)
      assert_equal('Rachel Abbott', book.authors[0])
      assert_equal('http://ecx.images-amazon.com/images/I/51BXhdGkBPL._SL110_.jpg', book.image)
      assert_equal('Kindle Edition', book.type)
      assert_equal('', book.list_price)
      assert_equal('', book.offer_price)
      assert_equal('', book.currency)
      assert_equal('471', book.page_count)
      assert_equal('2011-11-14', book.pub_date)
#      assert_equal({"asin"=>"B004TSXUWY",  "authors"=>["S J Watson"],  "title"=>"Before I Go To Sleep"}, book.suggested[0])

      book = nil
      assert_raises(NoMethodError) { book.title }
      book = Book.add(asin, @amazon_uk) if ! book = Book.get_asin(asin)
      assert_equal('Only the Innocent', book.title)
    end
  end

  def test_missing_description
    VCR.use_cassette('missing_description') do
      asin = '096416163X'
      refute_nil(book = @amazon_uk.get_book(asin))
      refute_nil(local = Book.add(book))
      assert_equal('Touch and Go', local.title)
      assert_nil(book['Items']['Item']['EditorialReviews'])
    end
  end

  def test_add_a_book_fail_duplicate
    VCR.use_cassette('duplicate_asin') do
      asin = '1870775791'
      refute_nil(book = @amazon_uk.get_book(asin))
      refute_nil(Book.add(book))
      assert_nil(Book.add(book))
    end
  end

end
