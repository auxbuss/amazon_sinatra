require_relative "test_helper"

class AmazonBestSellerTest < MiniTest::Unit::TestCase

  def test_we_have_data
    refute_equal(nil, bestsellers = AmazonBestSeller.find_by_locale('uk'))
    assert_equal('4444444444', Book.get_asin(bestsellers.books[4]).asin)
  end

  def test_for_missing_locale
    assert_nil(AmazonBestSeller.find_by_locale('de'))
  end

  def test_get_best_sellers_uk_missing_locale
    assert_nil(AmazonBestSeller.get_best_sellers('de'))
  end

  def test_get_best_seller_books_uk
    assert_kind_of(Array, books = AmazonBestSeller.get_best_sellers('uk'))
    assert_equal(10, books.count)
    assert_equal("Some title", books[5][:title])
  end

  def test_update_bestsellers
    VCR.use_cassette('amazon_best_sellers') do
      assert_equal(true, @amazon_uk.update_bestsellers)
      bestsellers = AmazonBestSeller.find_by(locale: 'uk')
      assert_kind_of(Book, top_book = Book.get_asin(bestsellers.books[0]))
    end
  end

  def setup;
    Book.delete_all
    AmazonBestSeller.delete_all
    bestsellers = AmazonBestSeller.new(locale: 'uk')
    bestsellers.save

    bestsellers = AmazonBestSeller.find_by_locale('uk')
    bestsellers.books = []
    asins = ['0000000000', '1111111111', '2222222222', '3333333333', '4444444444',
             '5555555555', '6666666666', '7777777777', '8888888888', '9999999999']
    asins.each do |asin|
      book = Book.create!(
        asin: asin, authors: ['Fred Blogs'], title: 'Some title', description: 'Some description',
        image: 'some_pic.jpg', reviews_url: 'http://reviews.com/', type: 'book type', suggested: [1,2,3,4],
        amazon_url: 'some url'
      )
      bestsellers.books << book.asin
    end
    bestsellers.save
    @amazon_uk = Amazon.new
  end

end
