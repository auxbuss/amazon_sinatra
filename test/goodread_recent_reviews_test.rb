require_relative "test_helper"

class GoodreadRecentReviewsTest < MiniTest::Unit::TestCase

  def test_get_recent_reviews
    assert_kind_of(Array, reviews = GoodreadRecentReviews.get_recent_reviews)
    assert_equal(20, reviews.count)
  end

end
