require_relative "test_helper"

class AmazonFindTest < MiniTest::Unit::TestCase

  def setup
    @amazon = Amazon.new
  end

  def test_find_one
    VCR.use_cassette('the_order_of_things') do
      result = @amazon.find('0679753354')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      assert_equal('The Order of Things (Vintage)', result["Item"][0]["ItemAttributes"]["Title"])
      assert_equal('The Order of Things (Vintage)', result.find_one('Title'))
      assert_equal('Michel Foucault', result.find_one('Author'))
    end
  end

  def test_find_one_large
    VCR.use_cassette('the_order_of_things_large') do
      result = @amazon.find('0679753354', :response_group => 'Large')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      assert_equal('The Order of Things (Vintage)', result["Item"][0]["ItemAttributes"]["Title"])
      assert_equal('The Order of Things (Vintage)', result.find_one('Title'))
      assert_equal('Michel Foucault', result.find_one('Author'))

      hashed = result.to_hash
      assert_equal(["OperationRequest", "Items"], hashed.keys)
      assert_equal(["Request", "Item"], hashed['Items'].keys)
      assert_equal(["ASIN",
 "DetailPageURL",
 "ItemLinks",
 "SalesRank",
 "SmallImage",
 "MediumImage",
 "LargeImage",
 "ImageSets",
 "ItemAttributes",
 "OfferSummary",
 "Offers",
 "CustomerReviews",
 "SimilarProducts",
 "BrowseNodes"],
        hashed['Items']['Item'].keys)
      assert_equal(["TotalOffers", "TotalOfferPages", "MoreOffersUrl"], hashed['Items']['Item']['Offers'].keys)
    end
  end

  def test_find_product_description
    VCR.use_cassette('infernal_devices_description') do
      result = @amazon.find('1407110934', :response_group => 'Large')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      assert result.find('EditorialReview')[0]['Content'].include?("Anchorage has become a static settlement on the shores of the Dead Continent")
      assert_equal('Philip Reeve', result.find_one('Author'))
      assert_equal('Infernal Devices (Mortal Engines Quartet)', result.find_one('Title'))

      result2 = @amazon.find('1407110934', :response_group => 'EditorialReview')
      assert_equal(true, result2.valid?)
      assert_equal(false, result2.has_errors?)
      assert result2.find('EditorialReview')[0]['Content'].include?("Anchorage has become a static settlement on the shores of the Dead Continent")
      assert_equal(["Product Description"], result2.find('Source'))
    end
  end

  def test_find_images
    VCR.use_cassette('the_order_of_things with images') do
      result = @amazon.find('0679753354', :response_group => 'Images')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      fred = Hash.from_xml(result.body)["ItemLookupResponse"]["Items"]["Item"]
      assert_equal(["ASIN", "SmallImage", "MediumImage", "LargeImage", "ImageSets"], fred.keys)
      hashed = result.to_hash
      assert_equal(["OperationRequest", "Items"], hashed.keys)
      assert_equal(["Request", "Item"], hashed['Items'].keys)
      assert_equal(["ASIN", "SmallImage", "MediumImage", "LargeImage", "ImageSets"], hashed['Items']['Item'].keys)
      assert_equal('http://ecx.images-amazon.com/images/I/31ZbK3qo6nL.jpg', result["Item"][0]['LargeImage']['URL'])
    end
  end

  def test_find_alternatives
    VCR.use_cassette('the_order_of_things_alternatives') do
      result = @amazon.find('0679753354', :response_group => 'AlternateVersions')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      hashed = result.to_hash
      assert_equal(["OperationRequest", "Items"], hashed.keys)
      assert_equal(["Request", "Item"], hashed['Items'].keys)
      assert_equal(["ASIN", "AlternateVersions"], hashed['Items']['Item'].keys)
      assert_equal(12, result.find('AlternateVersion').count)
    end
  end

#  def test_find_authority_title
#    VCR.use_cassette('the_order_of_things_authority_title') do
#      result = @amazon.find('0679753354', :response_group => 'RelatedItems', :relationship_type => 'AuthorityTitle')
#      result = @amazon.find('B000ASPUES', :response_group => 'RelatedItems', :relationship_type => 'AuthorityTitle')
#      assert_equal(true, result.valid?)
#      assert_equal(false, result.has_errors?)
#      ap result
#      hashed = result.to_hash
#      assert_equal(["OperationRequest", "Items"], hashed.keys)
#      assert_equal(["Request"], hashed['Items'].keys)
#      assert_equal(["ASIN", "AlternateVersions"], hashed['Items']['Item'].keys)
#      ap result.find('AlternateVersion')
#      assert_equal(10, result.find('AlternateVersion').count)
#    end
#  end

  def test_find_a_book_by_asin
    VCR.use_cassette('the_order_of_things') do
      result = @amazon.find('0679753354')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      assert_equal('The Order of Things (Vintage)', result["Item"][0]["ItemAttributes"]["Title"])
      assert_equal(['The Order of Things (Vintage)'], result.find('Title'))
    end
  end

  def test_find_an_asin
    VCR.use_cassette('the_order_of_things_2') do
      result = @amazon.find('0679753354')
      assert_equal('The Order of Things (Vintage)', result['Title'][0])
      result.each('Item') do |item|
        assert_equal('The Order of Things (Vintage)', item["ItemAttributes"]["Title"])
        assert_equal('0679753354', item['ASIN'])
      end
      result.each('ItemAttributes') do |item|
        assert_equal('The Order of Things (Vintage)', item["Title"])
      end

      fred = Hashie::Mash.new(result.to_hash)
      assert_equal('The Order of Things (Vintage)', fred.Items.Item.ItemAttributes.Title)

      barney = Hashie::Mash.new(result.find('ItemAttributes')[0])
      assert_equal('The Order of Things (Vintage)', barney.Title)
    end
  end

  def test_fail_to_find_an_asin
    VCR.use_cassette('invalid_asin') do
      result = @amazon.find('7738359834')
      assert_equal(true, result.has_errors?)
    end
  end

end
