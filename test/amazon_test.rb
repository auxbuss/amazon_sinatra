# encoding: utf-8

require_relative "test_helper"

class AmazonTest < MiniTest::Unit::TestCase

  def setup;
    Book.delete_all
    assert_kind_of(Amazon, @amazon_uk = Amazon.new)
  end

  def test_initialise
    assert_kind_of(Amazon, amazon = Amazon.new)
    assert_equal('auxbuss-21', amazon.params['AssociateTag'])
  end

  def test_invalid_locale
    assert_raises(Vacuum::BadLocale) { Amazon.new(:fred) }
    assert_kind_of(Amazon, Amazon.new(:uk))
    assert_kind_of(Amazon, Amazon.new(:us))
  end

  def test_get_similar
    asin = '043995178X'
    VCR.use_cassette('amazon_similarites') do
      suggestions = @amazon_uk.get_similar(asin)
      refute_equal(nil, suggestions)
      assert_kind_of(Array, suggestions)
      assert_equal(10, suggestions.count)
    end
  end

  def test_get_similar_2
    asin = '0385523882'
    VCR.use_cassette('amazon_similarites_2') do
      suggestions = @amazon_uk.get_similar(asin)
      refute_equal(nil, suggestions)
      assert_kind_of(Array, suggestions)
      assert_equal(10, suggestions.count)
    end
  end

  def test_get_similar_invalid_asin
    VCR.use_cassette('amazon_similarites_fail') do
      assert_nil(@amazon_uk.get_similar('121212'))
    end
  end

  def test_get_book
    VCR.use_cassette('amazon_get_book') do
      asin = '1407109081'
      refute_equal(nil, book = @amazon_uk.get_book(asin))
      assert_equal(["OperationRequest", "Items"], book.keys)
      assert_equal(["Request", "Item"], book['Items'].keys)
      assert_equal('1407109081', book['Items']['Item']['ASIN'])
      assert_equal(
["Author",
 "Binding",
 "EAN",
 "EANList",
 "Edition",
 "Feature",
 "ISBN",
 "ItemDimensions",
 "Label",
 "Languages",
 "ListPrice",
 "Manufacturer",
 "NumberOfItems",
 "NumberOfPages",
 "PackageDimensions",
 "ProductGroup",
 "ProductTypeName",
 "PublicationDate",
 "Publisher",
 "SKU",
 "Studio",
 "Title"],
        book['Items']['Item']['ItemAttributes'].keys)
      assert_equal(["ASIN",  "DetailPageURL",  "ItemLinks",  "SalesRank",  "SmallImage",  "MediumImage",
          "LargeImage",  "ImageSets",  "ItemAttributes",  "OfferSummary",  "Offers",  "CustomerReviews",
          "EditorialReviews",  "SimilarProducts",  "BrowseNodes"], book['Items']['Item'].keys)
      assert_equal(["TotalOffers", "TotalOfferPages", "MoreOffersUrl", "Offer"], book['Items']['Item']['Offers'].keys)
      assert_equal(["OfferAttributes", "OfferListing"], book['Items']['Item']['Offers']['Offer'].keys)
      assert_equal(["OfferListingId",  "Price",  "AmountSaved",  "PercentageSaved",  "Availability",  "AvailabilityAttributes",  "IsEligibleForSuperSaverShipping"],
          book['Items']['Item']['Offers']['Offer']['OfferListing'].keys)
      assert_equal("£3.86", book['Items']['Item']['Offers']['Offer']['OfferListing']['Price']['FormattedPrice'])
    end
  end

  def test_get_book_fail
    VCR.use_cassette('amazon_get_book_fail') do
      assert_nil(@amazon_uk.get_book('1212121212'))
    end
  end

  def test_alternative_versions
    VCR.use_cassette('amazon_alt_versions') do
      asin = '0753827875'
      refute_equal(nil, response = @amazon_uk.get_book(asin, 'AlternateVersions'))
      versions = response['Items']['Item']['AlternateVersions']['AlternateVersion']
      assert_equal(21, versions.count)
      assert_equal({"ASIN"=>"B002UP1SX6",  "Binding"=>"Kindle Edition",  "Title"=>"Call The Midwife: A True Story Of The East End In The 1950S"},
        versions.detect { |version| version['ASIN'] == 'B002UP1SX6' } )
      assert_equal(2, versions.select { |version| version['Binding']['Kindle'] }.count )
      assert_equal(7, versions.select { |version| version['Binding']['Paperback'] }.count )
    end
  end

  def test_power_search
    VCR.use_cassette('amazon_power_search') do
      refute_equal(nil, result = @amazon_uk.search('Books', :power => 'title:Call The Midwife and author:Jennifer Worth'))
      hash = result.to_hash
      assert_equal(["OperationRequest", "Items"], hash.keys)
      assert_equal(["Request", "TotalResults", "TotalPages", "MoreSearchResultsUrl", "Item"], hash['Items'].keys)
      assert_equal(10, hash['Items']['Item'].count)
    end
  end

  def test_get_best_sellers
    VCR.use_cassette('amazon_best_sellers_pull') do
      response = @amazon_uk.get_bestsellers
      assert_equal(Array, response.class)
      assert_equal(10, response.count)
    end
  end

end
