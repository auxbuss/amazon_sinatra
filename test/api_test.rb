require_relative "test_helper"
require 'rack/test'

class ApiTest < MiniTest::Unit::TestCase
  include Rack::Test::Methods

  def app
    Books::Api
  end

  def test_get_root
    get '/'
    assert last_response.ok?
    assert_equal 'Hello API!', last_response.body
  end

  def test_memcached
    memcache = Dalli::Client.new(nil, {:expires_in => 6.hours, :compress => true})
    memcache.set('abc', 123)
    assert_equal(123, memcache.get('abc'))
  end

  def test_get_author_page_check
    author_name = "S J Watson"
    author = URI::escape(author_name)
    assert_equal("S%20J%20Watson", author)
    get '/author/' + author + '/page/1'
    assert last_response.ok?
    VCR.use_cassette('author_api') do
      books = @amazon_uk.raw_book_search(author_name, 1)
      refute_nil(books)
      books['Item'].each do |book|
#        ap "#{book['ItemAttributes']['Author']} #{book['ASIN']} #{book['ItemAttributes']['Title']}"
      end
    end
  end

  def test_get_author_with_slash
    author = URI::escape("Joanna Neil/Janice Lynn")
    assert_equal("Joanna%20Neil Janice%20Lynn", author.gsub!(/\//, ' '))
    assert_equal("Joanna%20Neil Janice%20Lynn", author)
    get '/author/' + URI::escape(author) + '/page/1'
    assert_equal 404, last_response.status
    assert_equal({"result"=>"Author not found"}, JSON.parse(last_response.body))
  end

  def test_get_author_with_apostrophe
    author = URI::escape("Lily O'Brien")
    assert_equal("Lily%20O'Brien", author)
    get '/author/' + author + '/page/1'
    assert last_response.ok?
    assert_equal({"books"=>   [{"asin"=>"B005Q2MWVM",     "author"=>"Lily O'Brien",     "binding"=>"Kindle Edition",     "title"=>      "The Girl Nobody Wants: A Shocking True Story of Child Abuse in Ireland"}],  "total_books"=>1,  "total_pages"=>1}, JSON.parse(last_response.body))
  end

  def test_get_author_lower_case
    author = URI::escape("lily o'brien")
    assert_equal("lily%20o'brien", author)
    get '/author/' + author + '/page/1'
    assert last_response.ok?
    assert_equal({"books"=>   [{"asin"=>"B005Q2MWVM",     "author"=>"Lily O'Brien",     "binding"=>"Kindle Edition",     "title"=>      "The Girl Nobody Wants: A Shocking True Story of Child Abuse in Ireland"}],  "total_books"=>1,  "total_pages"=>1}, JSON.parse(last_response.body))
  end

  def test_get_author_causing_bug
    author = URI::escape("Mark Capell")
    assert_equal("Mark%20Capell", author)
    get '/author/' + author + '/page/1'
    assert last_response.ok?
    response = JSON.parse(last_response.body)
    assert_equal(
[{"asin"=>"B005LBFQBG",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>"RUN, RUN, RUN (crime thriller)"},
 {"asin"=>"B006NOC6WW",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>"RIOT MURDER - Myles Morgan Undercover"},
 {"asin"=>"B007FU7O1Q",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>"KILLER WHISPERS - Myles Morgan Undercover"},
 {"asin"=>"B009YN28JA",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>"VOWS to KILL"},
 {"asin"=>"B00B71SPT4",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>
   "MYLES UNDERCOVER... four adventures for the actor turned cop (Myles Morgan Undercover)"},
 {"asin"=>"B00B7202GW",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>"NO HERO - Myles Morgan Undercover"},
 {"asin"=>"B00B72066I",
  "author"=>"Mark Capell",
  "binding"=>"Kindle Edition",
  "title"=>"SHADOWS GROW - Myles Morgan Undercover"}],
      response['books'].sort_by { |book| book['asin'] })
    assert_equal(7, response['total_books'])
    assert_equal(1, response['total_pages'])
  end

  def test_get_author_page_1
    author = "Rachel%20Abbott"
    get '/author/' + author + '/page/1'
    assert last_response.ok?
  end

  def test_get_author_page_invalid_name
    author = "Invalid%20author%20name"
    get '/author/' + author + '/page/1'
    assert_equal 404, last_response.status
    assert_equal false, last_response.ok?
  end

  def test_get_asin
    get '/asin/7777777777'
    assert last_response.ok?
    assert_kind_of(String, last_response.body)
    jsoned = JSON.parse(last_response.body)
    assert_kind_of(Hash, jsoned)
    assert_equal 'Some title', jsoned['title']
    assert_equal 'Some description', jsoned['description']
  end

  def test_get_asin_fail
    get '/asin/12345'
    assert_equal(404, last_response.status)
    assert_equal 'ASIN not found', last_response.body
  end

  def setup;
    Book.delete_all
    AmazonBestSeller.delete_all
    bestsellers = AmazonBestSeller.new(locale: 'uk')
    bestsellers.save

    bestsellers = AmazonBestSeller.find_by_locale('uk')
    bestsellers.books = []
    asins = ['0000000000', '1111111111', '2222222222', '3333333333', '4444444444',
             '5555555555', '6666666666', '7777777777', '8888888888', '9999999999']
    asins.each do |asin|
      book = Book.create!(
        asin: asin, authors: ['Fred Blogs'], title: 'Some title', description: 'Some description',
        image: 'some_pic.jpg', reviews_url: 'http://reviews.com/', type: 'book type', suggested: [1,2,3,4],
        amazon_url: 'some url'
      )
      bestsellers.books << book._id
    end
    bestsellers.save
    assert_kind_of(Amazon, @amazon_uk = Amazon.new)
  end

end
