# encoding: utf-8

require_relative "test_helper"

class AuthorTest < MiniTest::Unit::TestCase

  def test_get_authors_books
    author_name = 'Philip Pullman'
    page = 1
    VCR.use_cassette('author_philip_pullman') do
      assert_kind_of(Author, author = Author.new(author_name))
      assert_equal(author.name, author_name)
      response = @amazon_uk.raw_book_search(author_name, page)
      assert_equal(false, response.invalid?)
      assert_equal(10, author.add_amazon_books(response, page).count)
      assert_kind_of(Hash, jsoned = JSON.parse(author.get_amazon_books(page)))
      assert_equal(3, jsoned.count)
      assert_equal(10, jsoned['books'].count)
#      assert_equal(945, author.total_books)
      assert_equal(103, author.total_pages)
      page = 2
      response = @amazon_uk.raw_book_search(author_name, page)
      assert_equal(false, response.invalid?)
      assert_equal(10, author.add_amazon_books(response, page).count)
      assert_kind_of(Hash, jsoned = JSON.parse(author.get_amazon_books(page)))
      assert_equal(10, jsoned['books'].count)
    end
  end

  def test_get_authors_books_lower_case
    author_name = "lily o'brien"
    page = 1
    VCR.use_cassette('author_lily_obrien') do
      assert_kind_of(Author, author = Author.new(author_name))
      assert_equal(author.name, author_name)
      response = @amazon_uk.raw_book_search(author_name, page)
      assert_equal(false, response.invalid?)
      assert_equal(1, author.add_amazon_books(response, page).count)
      assert_kind_of(Hash, jsoned = JSON.parse(author.get_amazon_books(page)))
      assert_equal(1, jsoned['books'].count)
      assert_equal(1, author.total_books)
      assert_equal(1, author.total_pages)
      assert_equal([{:asin=>"B005Q2MWVM", :author=>"Lily O'Brien", :binding=>"Kindle Edition",
            :title=> "The Girl Nobody Wants: A Shocking True Story of Child Abuse in Ireland"}],
            author.add_amazon_books(response, page))

      page = 2
      response2 = @amazon_uk.raw_book_search(author_name, page)
      assert_equal(true, response2.invalid?)

    end
  end

  def setup
    @amazon_uk = Amazon.new :uk
  end

end
