require_relative "test_helper"

class AmazonBrowseNodeTest < MiniTest::Unit::TestCase

  def test_browsenode_id_for_books
    VCR.use_cassette('experiment04') do
      result = @amazon.find_browse_node(266239, :response_group => 'TopSellers')
      assert_equal(false, result.has_errors?, "The response has errors")
      assert_equal(Array, result.find("TopSellers").class)
      assert_equal(10, result.find("TopSeller").count)
      assert_equal(20, result.find("ASIN").count)
    end
  end

  def test_browsenode_result_keys
    VCR.use_cassette('experiment04') do
      result = @amazon.find_browse_node(266239, :response_group => 'TopSellers')
      assert_equal(false, result.has_errors?, "The response has errors")
      hashed = result.to_hash
      assert_equal(["OperationRequest", "BrowseNodes"], hashed.keys )
      assert_equal(["HTTPHeaders", "RequestId", "Arguments", "RequestProcessingTime"], hashed["OperationRequest"].keys)
      assert_equal(["Request", "BrowseNode"], hashed["BrowseNodes"].keys)
      assert_equal(["IsValid", "BrowseNodeLookupRequest"], hashed["BrowseNodes"]["Request"].keys)
      assert_equal("True", hashed["BrowseNodes"]["Request"]['IsValid'])
      assert_equal({"BrowseNodeId"=>"266239", "ResponseGroup"=>"TopSellers"}, hashed["BrowseNodes"]["Request"]['BrowseNodeLookupRequest'])
      assert_equal(["BrowseNodeId", "Name", "TopSellers", "TopItemSet"], hashed["BrowseNodes"]["BrowseNode"].keys)

      assert_equal("266239", hashed["BrowseNodes"]["BrowseNode"]["BrowseNodeId"])
      assert_equal("Books", hashed["BrowseNodes"]["BrowseNode"]["Name"])

      assert_equal(["Type", "TopItem"], hashed["BrowseNodes"]["BrowseNode"]["TopItemSet"].keys)
      assert_equal("TopSellers", hashed["BrowseNodes"]["BrowseNode"]["TopItemSet"]["Type"])
      assert_kind_of(Array, hashed["BrowseNodes"]["BrowseNode"]["TopItemSet"]["TopItem"])
      assert_equal(10, hashed["BrowseNodes"]["BrowseNode"]["TopItemSet"]["TopItem"].count)
      assert_kind_of(Hash, hashed["BrowseNodes"]["BrowseNode"]["TopItemSet"]["TopItem"][0])
      assert_equal(["ASIN", "Title", "DetailPageURL", "ProductGroup", "Author"],
                                       hashed["BrowseNodes"]["BrowseNode"]["TopItemSet"]["TopItem"][0].keys)
      assert_equal(["TopSeller"], hashed["BrowseNodes"]["BrowseNode"]["TopSellers"].keys)
      assert_kind_of(Array, hashed["BrowseNodes"]["BrowseNode"]["TopSellers"]["TopSeller"])
      assert_equal(10, hashed["BrowseNodes"]["BrowseNode"]["TopSellers"]["TopSeller"].count)
      assert_kind_of(Hash, hashed["BrowseNodes"]["BrowseNode"]["TopSellers"]["TopSeller"][0])
      assert_equal(["ASIN", "Title"], hashed["BrowseNodes"]["BrowseNode"]["TopSellers"]["TopSeller"][0].keys)
    end
  end

  def test_browsenode_id_children
    VCR.use_cassette('experiment05') do
      result = @amazon.find_browse_node(266239, :response_group => 'BrowseNodeInfo')
      assert_equal(false, result.has_errors?, "The response has errors")
      assert_equal(10, result.find("BrowseNodeId").count)
    end
  end

  def setup;
    @amazon = Amazon.new
  end

end
