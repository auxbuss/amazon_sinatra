require_relative "test_helper"

class AmazonAuthorsTest < MiniTest::Unit::TestCase

  def test_search
    VCR.use_cassette('power_rachel_abbott') do
      result = @amazon.search('Books', :power => 'author:Rachel Abbott', :sort  => 'salesrank')
      assert_equal(false, result.has_errors?)
    end
  end

  def test_clean_author_name
    author_name = 'S J Watson'
    page = 1
    VCR.use_cassette('clean_author_name') do
      author = @amazon.get_author_with_books(author_name, page)
      assert_kind_of(Author, author)
      assert_kind_of(Hash, jsoned = JSON.parse(author.get_amazon_books(page)))
      assert_equal(10, jsoned['books'].count)
      jsoned['books'].each do |book|
        assert_equal(10, book['asin'].length)
      end
    end
  end

  def test_get_author_abbott_page_1
    author_name = 'Rachel Abbott'
    page = 1
    VCR.use_cassette('rachel_abbott_get') do
      author = @amazon.get_author_with_books(author_name, page)
      assert_kind_of(Author, author)
      assert_kind_of(Hash, jsoned = JSON.parse(author.get_amazon_books(page)))
      assert_equal(2, jsoned['books'].count)
      jsoned['books'].each do |book|
        assert_equal(10, book['asin'].length)
      end
    end
  end

  def test_get_author_pullman_page_1
    author_name = 'Philip Pullman'
    page = 1
    VCR.use_cassette('philip_pullman_get') do
      author = @amazon.get_author_with_books(author_name, page)
      assert_kind_of(Author, author)
      assert_kind_of(Hash, jsoned = JSON.parse(author.get_amazon_books(page)))
      assert_equal(10, jsoned['books'].count)
      jsoned['books'].each do |book|
        assert_equal(10, book['asin'].length)
      end
    end
  end

  def test_get_author_invalid_author_1
    author = 'I Dont Exist'
    page = 1
    VCR.use_cassette('invalid_author_get') do
      books = @amazon.get_author_with_books(author, page)
      assert_nil(books)
    end
  end

  def test_it_gets_rachel_abbott_books
    VCR.use_cassette('rachel_abbott') do
      result = @amazon.search('Rachel Abbott')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      hashed = result.to_hash
      assert_equal(["OperationRequest", "Items"], hashed.keys)
      assert_equal(["HTTPHeaders", "RequestId", "Arguments", "RequestProcessingTime"], hashed["OperationRequest"].keys)
      assert_equal(["Request", "TotalResults", "TotalPages", "MoreSearchResultsUrl", "Item"], hashed["Items"].keys)
      assert_equal(8, hashed["Items"]['TotalPages'].to_i)
      assert_equal(["ASIN", "DetailPageURL", "ItemLinks", "ItemAttributes"], hashed["Items"]["Item"][0].keys)
      assert_equal(["Author", "Manufacturer", "ProductGroup", "Title"], hashed["Items"]["Item"][0]["ItemAttributes"].keys)
      assert_equal(10, hashed["Items"]["Item"].count)
      result.each('Item') do |item|
        assert_equal(10, item['ASIN'].length)
      end
    end
  end

  def test_it_gets_philip_pullman_books
    VCR.use_cassette('philip_pullman') do
      result = @amazon.search('Books', { author: 'Philip Pullman', ItemPage: 1, Keywords: 'Book', ResponseGroup: 'Large', Sort: 'salesrank' })
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      hashed = result.to_hash
      assert_equal(["Request", "TotalResults", "TotalPages", "MoreSearchResultsUrl", "Item"], hashed["Items"].keys)
      assert_equal(101, hashed["Items"]['TotalPages'].to_i)
      assert_equal(["ASIN",
 "DetailPageURL",
 "ItemLinks",
 "SalesRank",
 "SmallImage",
 "MediumImage",
 "LargeImage",
 "ImageSets",
 "ItemAttributes",
 "OfferSummary",
 "Offers",
 "CustomerReviews",
 "EditorialReviews",
 "SimilarProducts",
 "BrowseNodes"], hashed["Items"]["Item"][0].keys)
      assert_equal(10, hashed["Items"]["Item"].count)
      result.each('Item') do |item|
        assert_equal(10, item['ASIN'].length)
      end
      assert_equal('1846140269', hashed["Items"]["Item"][0]['ASIN'])
    end
  end

  def test_it_gets_philip_pullman_books_page_2
    VCR.use_cassette('philip_pullman_2') do
      result = @amazon.search('Books', { Author: 'Philip Pullman', ItemPage: 2 })
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      hashed = result.to_hash
      assert_equal(103, hashed["Items"]['TotalPages'].to_i)
      assert_equal(10, hashed["Items"]["Item"].count)
      result.each('Item') do |item|
        assert_equal(10, item['ASIN'].length)
      end
    end
  end

  def setup;
    @amazon = Amazon.new
  end
end
