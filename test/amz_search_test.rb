require_relative "test_helper"

class AmazonSearchTest < MiniTest::Unit::TestCase

  def setup
    @amazon = Amazon.new
  end

  def test_bestsellers
    VCR.use_cassette('experiment01') do
      result = @amazon.search('Books', :BrowseNode => 1025612, :ItemPage  => 7, :Sort  => 'salesrank')
      assert_equal(false, result.has_errors?, "The response has errors")
    end
  end

  def test_search
    VCR.use_cassette('experiment02') do
      result = @amazon.search('Books', :power => 'author:deleuze', :sort  => 'salesrank')
      assert_equal(false, result.has_errors?, "The response has errors")
    end
  end

  def test_raw_search_for_books
    author_name = 'Rachel Abbott'
    page = 1
    VCR.use_cassette('search_books_abbott_raw') do
      result = @amazon.raw_search_for_books(author_name, page)
      assert_equal(false, result.invalid?)
      hashed = result.to_hash
      assert_equal(["OperationRequest", "Items"], hashed.keys)
      assert_equal(["Request", "TotalResults", "TotalPages", "MoreSearchResultsUrl", "Item"], hashed["Items"].keys)
      assert_equal(6, hashed["Items"]['TotalPages'].to_i)
      assert_equal(["ASIN",  "DetailPageURL",  "ItemLinks",  "SalesRank",  "SmallImage",  "MediumImage",  "LargeImage",
          "ImageSets",  "ItemAttributes",  "CustomerReviews",  "EditorialReviews",  "SimilarProducts",  "BrowseNodes"],
          hashed['Items']['Item'][0].keys)
      assert_equal(["Author",  "Binding",  "Edition",  "Format",  "IsAdultProduct",  "Label",  "Languages",  "Manufacturer",  "NumberOfItems",  "NumberOfPages",  "ProductGroup",  "ProductTypeName",  "PublicationDate",  "Publisher",  "ReleaseDate",  "Studio",  "Title"], hashed['Items']['Item'][0]['ItemAttributes'].keys)
    end
  end

  def test_search_for_books
    author_name = 'Rachel Abbott'
    page = 1
    VCR.use_cassette('search_books_abbott') do
      result = @amazon.search_for_books(author_name, page)
      assert_kind_of(Hash, result)
      assert_equal(6, result[:total_pages])
      assert_equal(5, result[:books].count)
      assert_equal(0, Book.where(authors: author_name).count)
    end
  end

  def test_it_gets_mark_capell_books
    author_name = 'Mark Capell'
    title = "RUN, RUN, RUN (crime thriller)"
    page = 1
    VCR.use_cassette('mark_capell') do
      result = @amazon.raw_book_search(author_name, page)
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      assert_equal(27, result['Title'].count)
      assert_equal(title, result["Title"][0])
      hashed = result.to_hash
      assert_equal(["Request", "TotalResults", "TotalPages", "MoreSearchResultsUrl", "Item"], hashed["Items"].keys)
      assert_equal("7", hashed["Items"]["TotalResults"] )
      assert_equal(["ASIN",  "DetailPageURL",  "ItemLinks",  "SalesRank",  "SmallImage",  "MediumImage",
          "LargeImage",  "ImageSets",  "ItemAttributes",  "CustomerReviews",  "EditorialReviews",
          "SimilarProducts",  "BrowseNodes"], hashed["Items"]["Item"][0].keys)
      assert_equal(
["Author",
 "Binding",
 "Format",
 "IsAdultProduct",
 "Label",
 "Languages",
 "Manufacturer",
 "NumberOfItems",
 "NumberOfPages",
 "ProductGroup",
 "ProductTypeName",
 "PublicationDate",
 "Publisher",
 "ReleaseDate",
 "Studio",
 "Title"], hashed["Items"]["Item"][0]["ItemAttributes"].keys)
      assert_equal(title, hashed["Items"]["Item"][0]["ItemAttributes"]["Title"])

      result.each('Item') do |item|
        assert_equal(10, item['ASIN'].length)
      end
    end
  end

  def test_it_gets_orwells_books
    VCR.use_cassette('orwell') do
      result = @amazon.search('George Orwell')
      assert_equal(true, result.valid?)
      assert_equal(false, result.has_errors?)
      hashed = result.to_hash
      assert_equal(["OperationRequest", "Items"], hashed.keys)
      assert_equal(["HTTPHeaders", "RequestId", "Arguments", "RequestProcessingTime"], hashed["OperationRequest"].keys)
      assert_equal(["Request", "TotalResults", "TotalPages", "MoreSearchResultsUrl", "Item"], hashed["Items"].keys)
      assert_equal(["ASIN", "DetailPageURL", "ItemLinks", "ItemAttributes"], hashed["Items"]["Item"][0].keys)
      assert_equal(["Author", "Manufacturer", "ProductGroup", "Title"], hashed["Items"]["Item"][0]["ItemAttributes"].keys)
      assert_equal("Nineteen Eighty-Four", hashed["Items"]["Item"][0]["ItemAttributes"]["Title"])
      assert_equal("Nineteen Eighty-Four", result["Item"][0]["ItemAttributes"]["Title"])

      result.each('Item') do |item|
        assert_equal(10, item['ASIN'].length)
      end
    end
  end

  def self.startup
    Book.delete_all
    AmazonBestSeller.delete_all
    bestsellers = AmazonBestSeller.new(locale: 'uk')
    bestsellers.save
    bestsellers = AmazonBestSeller.find_by_locale('uk')
    bestsellers.books = []
    asins = ['0000000000', '1111111111', '2222222222', '3333333333', '4444444444',
             '5555555555', '6666666666', '7777777777', '8888888888', '9999999999']
    asins.each do |asin|
      book = Book.create!(
        asin: asin, authors: ['Fred Blogs'], title: 'Some title', description: 'Some description',
        image: 'some_pic.jpg', reviews_url: 'http://reviews.com/', type: 'book type', suggested: [1,2,3,4],
        amazon_url: 'some url'
      )
      bestsellers.books << book.asin
    end
    bestsellers.save
  end

end
