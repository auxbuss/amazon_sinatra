#!/usr/bin/env ruby

ENV["RACK_ENV"] ||= "production"
require_relative "./../config/boot"
p ENV["RACK_ENV"]

Moped.logger = Logger.new($stdout)

locale = :uk

AmazonBestSeller.delete_all
bestsellers = AmazonBestSeller.new(locale: locale)
bestsellers.save
