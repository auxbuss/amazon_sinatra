#!/usr/bin/env ruby

ENV["RACK_ENV"] ||= "production"
require_relative "./../config/boot"
p ENV["RACK_ENV"]

Moped.logger = Logger.new($stdout)

locale = :uk

if ! amazon = Amazon.new(locale) then
  p "failed to create Amazon #{locale} connection. Aborting"
  exit 1
end

if ! amazon.update_bestsellers then
  p "failed to update Amazon #{locale} bestsellers. Aborting"
  exit 1
end

puts Book.count
puts Book.delete_all(conditions: {'updated_at' => {'$lt' => 1.days.ago}})
puts Book.count

exit 0
