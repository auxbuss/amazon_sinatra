#!/usr/bin/env ruby

require File.expand_path(File.dirname(__FILE__) + "/../config/boot")

p ENV["RACK_ENV"]

Moped.logger = Logger.new($stdout)

begin
  goodreads = Goodread.new
rescue
  p "Failed to create Goodread object. Aborting."
  exit 1
end

if ! goodreads.update_recent_reviews then
  p "failed to update Goodread recent reviews. Aborting."
  exit 1
end

exit 0
