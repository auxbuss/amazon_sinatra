ENV["RACK_ENV"] ||= "development"

require 'bundler/setup'
Bundler.setup
Bundler.require(:default, ENV["RACK_ENV"].to_sym)

require_relative "./../main/main"
require_relative "./../api/api"
require_relative "./../models/init"
require_relative "./../lib/init"

Mongoid.load!(File.dirname(__FILE__) + "/mongoid.yml")
